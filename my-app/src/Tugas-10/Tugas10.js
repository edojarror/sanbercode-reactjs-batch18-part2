import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]


class Tugas10 extends React.Component {
    render() {
        console.log(this.props)
        return (
            <div>
                <Judul />
                <div style={{border: "2px solid black", margin: "2rem 1.1rem 2rem 1.1rem"}} >
                    <TabelHargaBuah />
                    <TabelSemangka dataHargaBuah={dataHargaBuah} />
                </div>
                

            </div>
            
        )
      
    }
}

class Judul extends React.Component {
    render() {
        return (
            <h1 style={{textAlign: "center", fontSize: "3em"}}>Tabel Harga Buah</h1>
        )
    }
}

class TabelHargaBuah extends React.Component {
    render() {
        const mainDiv = {display: "flex"};
        const tabelDiv = {border: "2px solid white", width: "33.3%", textAlign: "center", backgroundColor: "#a6a6a6",fontSize: "0.95rem" , fontWeight: "500"}

        return (
            <div style={mainDiv}>
                <div style={tabelDiv}>Nama</div>
                <div style={tabelDiv}>Harga</div>
                <div style={tabelDiv}>Berat</div>
            </div>
            
        )
    }
}

class TabelSemangka extends React.Component {
    render() {
        console.log(this.props)
        const semangkaDiv = {display: "flex"};
        const tabelDiv = {border: "2px solid white", width: "33.3%", backgroundColor: "#ff8c5c",fontSize: "0.95rem" , fontWeight: "500"}
        return this.props.dataHargaBuah.map((buah, index) => {
                    return (
                        <div key={index} style={semangkaDiv}>
                            <div style={tabelDiv}>{buah.nama}</div>
                            <div style={tabelDiv}>{buah.harga}</div>
                            <div style={tabelDiv}>{`${buah.berat /1000} Kg`}</div>
                        </div>
                    )
                })
            
    }
}




export default Tugas10;