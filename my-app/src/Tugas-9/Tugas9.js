import React from 'react';

class Tugas9 extends React.Component {
    render() {
        const divRoot = {border: "1px solid black", margin: "1em", borderRadius: "12px"}
        const judul = {marginTop: "2em", fontSize: "3em"}
        const labelInputNama = {margin: "0 6em 0 2em", padding: "0" ,fontSize: "1em", fontWeight: "800"}
        const divInputName = {display: "flex"}
        const divContent = {display: "flex", margin: "5em 0 2em 0"}
        const inputSemangka = {marginLeft: "-0.4em", marginTop: "2em"}
        const inputJeruk = {marginLeft: "-2.7em", marginTop: "0.3em"}
        const inputNanas = {marginLeft: "-2.2em", marginTop: "0.3em"}
        const inputSalak = {marginLeft: "-2.7em", marginTop: "0.3em"}
        const inputAnggur = {display: "inline-block",display: "flex", marginTop: "0.3em"}
        const labelDaftarItem = {fontSize: "1em", fontWeight: "800", marginLeft: "2em", marginRight: "8.55em"}
        const button = {display: "block",backgroundColor: "white", marginLeft: "2em",marginTop: "2em",fontSize: "1em" ,border: "2px solid black" ,borderRadius: "12px"}
        return (
            <div className="App" style={divRoot}>
            <h1 style={judul}>Form Pembelian Buah</h1>
        
            <div style={divContent}>
                <form>

                <div style={divInputName}>
                <label htmlFor="input nama" id="nama" style={labelInputNama}>Nama Pelanggan</label> 
                <input type="text" size="45"></input>
                </div>
                <div style={inputSemangka}>
                    <input type="checkbox" id="semangka" name="semangka" value="semangka"></input>
                    <label htmlFor="semangka">Semangka</label>
                </div>
                <div style={inputJeruk}>
                    <input type="checkbox" id="jeruk" name="jeruk" value="jeruk"></input>
                    <label htmlFor="jeruk">Jeruk</label>
                </div>
                <div style={inputNanas}>
                    <input type="checkbox" id="nanas" name="nanas" value="nanas"></input>
                    <label htmlFor="nanas">Nanas</label>
                </div>
                <div style={inputSalak}>
                    <input type="checkbox" id="salak" name="salak" value="salak"></input>
                    <label htmlFor="salak">Salak</label>
                </div>
                <div style={inputAnggur}>
                    <label htmlFor="daftar-item" id="item" style={labelDaftarItem}>Daftar Item</label>
                    <input type="checkbox" id="anggur" name="anggur" value="anggur"></input>
                    <label htmlFor="anggur">Anggur</label>
                </div>
                <button type="submit" style={button}>Kirim</button>

                </form> 
            </div>
            
            </div>
        );
    }
}



export default Tugas9;