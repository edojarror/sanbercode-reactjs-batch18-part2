import React from 'react';
import logo from './logo.svg';
import './App.css';
import Tugas9 from './Tugas-9/Tugas9';
import Tugas10 from './Tugas-10/Tugas10';
import Tugas11 from './Tugas-11/Tugas11';



function App() {
  return (
    <div>
      <Tugas11 />
      <Tugas10 />
      <Tugas9 />
    </div>
    
  )
}

export default App;
