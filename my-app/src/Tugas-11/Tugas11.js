import React from 'react';

class Tugas11 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 100,
            date: new Date()
        };
    }
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );  
    }

    tick() {
        if(this.state.time === 0) {
            clearInterval(this.timerID); 
        } else {
            this.setState({
                time: this.state.time - 1,
                date: new Date()
            });
        } 
        
    }
    componentWillUnmount() { 
        clearInterval(this.timerID); 
        
                
    }
    
    render() {
        return (
            <div>
                {this.state.time === 0 
                ? null 
                : <h1 style={{textAlign: "center"}}>
                    sekarang jam: {this.state.date.toLocaleTimeString()} 
                    <span style={{marginLeft: "10em"}}> hitung mundur :{this.state.time}
                    </span></h1> 
                }
            </div>
        )
    }
}



export default Tugas11;